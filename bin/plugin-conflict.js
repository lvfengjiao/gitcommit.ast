const { getfiles, exitPreCommit } = require("./commander");
const fs = require("fs");
const colors = require("colors");
const pluginConflict = (key) => {
  const filenames = getfiles();
  let files = [];
  const result = filenames.some((file) => {
    if (file == null) return false;
    const code = fs.readFileSync(file, "utf-8");
    const re =
      /[<]{6,}[^<|^=|^>]+[=]{6,}[^>|^=|^<]+[=]{6,}[^>|^=|^<]+[>]{6,}/gm;
    const result = re.test(code);
    if (result) {
      files.push(`${file} has conflict`);
    }
    return result;
  });
  !result && console.log(`****precommit ${key} success!`.green);
  result && exitPreCommit(files);
  return Promise.resolve(!result);
};
module.exports = { pluginConflict };
