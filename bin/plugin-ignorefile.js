const { getfiles, exitPreCommit } = require("./commander");
const colors = require("colors");
const pluginIgnoredFiles = (config) => {
  const { ignoreCase, fileList } = config;
  const files = getfiles();
  const errors = [];
  const result = fileList.every((file) => {
    const rule = ignoreCase ? "ig" : "g";
    const re = new RegExp(file, rule);
    const result = files.some((i) => {
      return re.test(i);
    });
    if (result) {
      errors.push(`${file} fail to ignore`);
    }
    return !result;
  });
  if (result) console.log("****precommit ignore success".green);
  !result && exitPreCommit(errors);
  return !result;
};
module.exports = { pluginIgnoredFiles };
