const types = require("babel-types");
const colors = require("colors");
const ForStatement = (file, maxdepth, key) => (path) => {
  const { node } = path;

  let maxthDepth = 1;
  const checkdepth = (node, length) => {
    const { type, body } = node;
    if (type === "ForStatement") {
      length += 1;
      if (maxthDepth < length) maxthDepth = length;
    }
    if (body == null) {
      if (maxthDepth < length) maxthDepth = length;
      return;
    } else if (Array.isArray(body)) {
      for (let element of body) {
        checkdepth(element, length);
      }
    } else {
      checkdepth(body, length);
    }
  };
  checkdepth(node, 0);
  let str = "";
  if (maxthDepth > +maxdepth) {
    str = `${key}${file}->${maxthDepth}${key}`;
  }

  let func = types.expressionStatement({
    type: "StringLiteral",
    value: str,
  });
  console.log(`checking for ${file} -> ${maxthDepth}`.yellow);
  path.replaceWith(func);
};
module.exports = { ForStatement };
