const { execSync } = require("child_process");
const colors = require("colors");
const exitPreCommit = (...reason) => {
  reason && console.log(JSON.stringify(reason).red);
  // 退出pre-commit, 非0代表异常退出 0 会继续进入后续程序
  // https://nodejs.org/api/process.html#process_process_exit_code
  process.exit(-1);
};
const getCommandRetVal = (command) => {
  return execSync(command)
    .toString("utf-8")
    .trim()
    .split("\n")
    .filter((i) => {
      return i.startsWith("D") === false;
    })
    .map((i) => {
        return i.split(/\s/g)[1]
    });
};

const checkTarget = (dirs, filename) => {
  return dirs.some((dir) => {
    const re = new RegExp(dir, "g");
    return re.test(filename);
  });
};

const getfiles = () => {
    return execSync('git ls-files').toString("utf-8")
    .trim().split('\n')
}
module.exports = {
  getfiles,
  checkTarget,
  getCommandRetVal,
  exitPreCommit
};