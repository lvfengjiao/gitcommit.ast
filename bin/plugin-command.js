const { exec } = require("child_process");
const colors = require("colors");
const pluginCommand = (type, config) => {
  const { run } = config;
  return new Promise((resolve, reject) => {
    exec(run, (err, stdout) => {
      if (err) {
        console.log(`Run command ${type} ${run} Exception ${err}`.red);
        resolve(false);
      }
      console.log(JSON.stringify({ stdout: stdout }).green);
      resolve(true);
    });
  });
};
module.exports = { pluginCommand };
