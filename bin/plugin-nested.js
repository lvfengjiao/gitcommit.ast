const { getfiles, checkTarget } = require("./commander");
const { nested } = require("./check-ast");
const colors = require("colors");
const nestedPlugin = (key, config, target) => {
  const filenames = getfiles();
  const type = key.replace("nested", "").toLowerCase();
  const { test, maxdepth } = config;
  const result = filenames.every((file) => {
    if (checkTarget(target, file)) {
      const re = new RegExp(test, "ig");
      if (re.test(file) === true) {
        // a weird bug about re
        return nested(file, type, maxdepth);
      }
      return true;
    } else {
      return true;
    }
  });
  result && console.log(`****precommit ${key} success!`.green);
  return result;
};
module.exports = { nestedPlugin };
