const colors = require("colors");
const babel = require("@babel/core");
const fs = require("fs");
const { ForStatement } = require("./check-nested-for");
const { IfStatement } = require("./check-nested-if");
const { FunctionDeclaration } = require("./checkAngularBestPractice");
const { exitPreCommit } = require("./commander");

const nested = (file, type, maxdepth) => {
  if (file == null || type == null) throw new Error("Missing file/file");
  const code = fs.readFileSync(file, "utf-8");
  const key = `nested_${+new Date()}`;
  let visitor = {};
  if (type == "if") {
    visitor = {
      IfStatement: IfStatement(file, maxdepth, key),
    };
  }
  if (type == "for") {
    visitor = {
      ForStatement: ForStatement(file, maxdepth, key),
    };
  }
  if (type == "angular") {
    visitor = {
      FunctionDeclaration: FunctionDeclaration(file, maxdepth, key),
    };
  }
  let { code: transformCode } = babel.transform(code, {
    plugins: [
      "@babel/plugin-transform-typescript",
      {
        visitor,
      },
    ],
  });
  if (transformCode.indexOf(key) !== -1) {
    const start = transformCode.indexOf(key) + key.length;
    const end = transformCode.lastIndexOf(key);
    const substr = transformCode.substring(start, end);
    const [filename, depth] = substr.split("->");
    exitPreCommit({ filename, depth, error: `nested ${type} error` });
    return false;
  }
  return true;
};
module.exports = { nested };
