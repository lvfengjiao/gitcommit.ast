const types = require("babel-types");
const colors = require("colors");
const IfStatement = (file, maxdepth, key) => (path) => {
  const { node } = path;

  let maxthDepth = 1;
  const checkdepth = (node, length) => {
    const { type, consequent } = node;
    if (type === "IfStatement") {
      length += 1;
      if (maxthDepth < length) maxthDepth = length;
    } else if (type == "ExpressionStatement") {
      return;
    }
    if (consequent == null) {
      if (maxthDepth < length) maxthDepth = length;
      return;
    } else if (consequent.type === "BlockStatement") {
      const { body } = consequent;
      if (body == null) return;
      for (let element of body) {
        checkdepth(element, length);
      }
    } else if (consequent.type === "IfStatement") {
      checkdepth(consequent, length);
    }
  };
  checkdepth(node, 0);
  let str = "";
  if (maxthDepth > +maxdepth) {
    str = `${key}${file}->${maxthDepth}${key}`;
  }

  let func = types.expressionStatement({
    type: "StringLiteral",
    value: str,
  });
  console.log(`checking if ${file} -> ${maxthDepth}`.yellow);
  path.replaceWith(func);
};
module.exports = { IfStatement };
