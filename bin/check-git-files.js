const colors = require("colors");
const { getconfig } = require("./getconfig");
const which = require("which");
const { pluginConflict } = require("./plugin-conflict");
const { pluginIgnoredFiles } = require("./plugin-ignorefile");
const { nestedPlugin } = require("./plugin-nested");
const { pluginRequiredFiles } = require("./plugin-requiredfile");
const spawn = require("cross-spawn");

const config = getconfig();

const { plugins } = config;
const callplugin = async (type, config, target) => {
  switch (type) {
    case "nestedIf":
    case "nestedFor":
    case "angular": 
      return await nestedPlugin(type, config, target);
    case "ignore":
      return await pluginIgnoredFiles(config);
    case "required":
      return await pluginRequiredFiles(config);
    case "unittest": {
      const result = spawn.sync(
        which.sync("git"),
        ["rev-parse", "--show-toplevel"],
        {
          stdio: "pipe",
          encoding: "utf-8",
        }
      );
      spawn(which.sync("npm"), ["run", "test"], {
        env: process.env,
        cwd: result.stdout.toString().trim(),
        stdio: [0, 1, 2],
      }).once("close", function closed(code) {
        if (code) console.log(code);
      });
      return true;
    }
    case "conflictCheck":
      const result = await pluginConflict(type, config);
      return result;
    default:
      return false;
  }
};
const result = Object.keys(plugins).every(async (plguinkey) => {
  const conf = plugins[plguinkey];
  if (conf === false) return true; // skip the check
  return await callplugin(plguinkey, conf, config.target);
});

result && console.log("****pre check sussess".green);
