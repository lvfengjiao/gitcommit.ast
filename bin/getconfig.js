const fs = require("fs");
const path = require("path");
const colors = require("colors");
const getconfig = () => {
  const __dirname = path.resolve(path.dirname(""));
  const localpath = path.resolve(__dirname, ".checkrc");
  const defaultPath = path.resolve(
    "node_modules",
    "gitcommit.ast",
    "./.checkrc"
  );

  let defaultJson = {};
  try {
    defaultJson = JSON.parse(fs.readFileSync(defaultPath));
  } catch (e) {
    console.error("default config error", e);
  }
  if (fs.existsSync(localpath)) {
    try {
      const custom = JSON.parse(fs.readFileSync(localpath));
      return { ...defaultJson, ...custom };
    } catch (e) {
      console.error("custom config error", e);
    }
  }
  return defaultJson;
};
module.exports = { getconfig };
