"use strict";

//
// Compatibility with older node.js as path.exists got moved to `fs`.
//
const fs = require("fs");
const path = require("path");
const os = require("os");

const hook = path.join(__dirname, "hook");
const root = path.resolve(__dirname, "..", "..");
const exists = fs.existsSync || path.existsSync;

//
// Gather the location of the possible hidden .git directory, the hooks
// directory which contains all git hooks and the absolute location of the
// `pre-commit` file. The path needs to be absolute in order for the symlinking
// to work correctly.
//
const git = path.resolve(root, ".git"),
  hooks = path.resolve(git, "hooks"),
  precommit = path.resolve(hooks, "pre-commit"),
  commitmsg = path.resolve(hooks, "commit-msg");

// if (!exists(hooks)) fs.mkdirSync(hooks);

//
// If there's an existing `pre-commit` hook we want to back it up instead of
// overriding it and losing it completely as it might contain something
// important.
//
if (exists(precommit) && !fs.lstatSync(precommit).isSymbolicLink()) {
  console.log("pre-commit:");
  console.log("pre-commit: Detected an existing git pre-commit hook");
  fs.writeFileSync(precommit + ".old", fs.readFileSync(precommit));
  console.log("pre-commit: Old pre-commit hook backuped to pre-commit.old");
  console.log("pre-commit:");
}
if (exists(commitmsg) && !fs.lstatSync(commitmsg).isSymbolicLink()) {
  console.log("commit-msg:");
  console.log("commit-msg: Detected an existing git commit-msg hook");
  fs.writeFileSync(commitmsg + ".old", fs.readFileSync(commitmsg));
  console.log("commit-msg: Old commit-msg hook backuped to commit-msg.old");
  console.log("commit-msg:");
}

//
// We cannot create a symlink over an existing file so make sure it's gone and
// finish the installation process.
//
try {
  fs.unlinkSync(precommit);
} catch (e) {}
try {
  fs.unlinkSync(commitmsg);
} catch (e) {}

// Create generic precommit hook that launches this modules hook (as well
// as stashing - unstashing the unstaged changes)
// TODO: we could keep launching the old pre-commit scripts
var hookRelativeUnixPath = os.platform().includes("win")
  ? hook.replace(root, ".").replace(/\\/g, "\\\\")
  : hook.replace(root, ".");

var precommitContent =
  "#!/bin/bash" +
  os.EOL +
  hookRelativeUnixPath +
  os.EOL +
  "RESULT=$?" +
  os.EOL +
  "[ $RESULT -ne 0 ] && exit 1" +
  os.EOL +
  "exit 0" +
  os.EOL;

//
// It could be that we do not have rights to this folder which could cause the
// installation of this module to completely fail. We should just output the
// error instead destroying the whole npm install process.
//
try {
  fs.writeFileSync(precommit, precommitContent);
} catch (e) {
  console.error("pre-commit:");
  console.error(
    "pre-commit: Failed to create the hook file in your .git/hooks folder because:"
  );
  console.error("pre-commit: " + e.message);
  console.error("pre-commit: The hook was not installed.");
  console.error("pre-commit:");
}

const commitContent =
  "#!/bin/bash" +
  os.EOL +
  `COMMIT_FILE=$1` +
  os.EOL +
  `COMMIT_MSG=$(cat $1)` +
  os.EOL +
  `JIRA_ID=$(echo "$COMMIT_MSG" | grep -Eo "[A-Z0-9]{4,10}-[0-9]+")` +
  os.EOL +
  `if [ -z "$JIRA_ID" ]; then` +
  os.EOL +
      `branchmore=$(git branch | grep "*")` +
  os.EOL +
      `branch=$(echo "$branchmore"/* /)` +
  os.EOL +
      `jira_inside_id=$(echo "$branch" | grep -Eo "[A-Z0-9]{4,10}-[0-9]+")` +
  os.EOL +
      `if [ -z "$jira_inside_id" ]; then` +
  os.EOL +
  `         echo "$MSG_WITHOUT_COMMENT@$JIRA_ID" > $COMMIT_FILE` +
  os.EOL +
  `         echo "JIRA ID '$JIRA_ID', is INVALID JIRA ID. (Use --no-verify to skip)"` +
  os.EOL +
  `	       exit 1` +
  os.EOL +
      `else` +
  os.EOL +
  `       echo "$jira_inside_id $COMMIT_MSG" > "$1"` +
  os.EOL +
      `fi` +
  os.EOL +
  `fi` +
  os.EOL;
try {
  fs.writeFileSync(commitmsg, commitContent);
} catch (e) {
  console.error("commit-msg:");
  console.error(
    "commit-msg: Failed to create the hook file in your .git/hooks folder because:"
  );
  console.error("commit-msg: " + e.message);
  console.error("commit-msg: The hook was not installed.");
  console.error("commit-msg:");
}

try {
  fs.chmodSync(precommit, "777");
} catch (e) {
  console.error("pre-commit:");
  console.error(
    "pre-commit: chmod 0777 the pre-commit file in your .git/hooks folder because:"
  );
  console.error("pre-commit: " + e.message);
  console.error("pre-commit:");
}
try {
  fs.chmodSync(commitmsg, "777");
} catch (e) {
  console.error("commit-msg:");
  console.error(
    "commit-msg: chmod 0777 the commitmsg file in your .git/hooks folder because:"
  );
  console.error("commit-msg: " + e.message);
  console.error("commit-msg:");
}
