** Configurable git precommit ast **
# Description
The aim of this library is to train developer coding standard. 
When you execuate `git commit -m "xxxx"`, below points will be execuated.
1. performance: checking if there's too much nested for/if
2. clean code: ckecking if some file should be ignored like package-lock.json
3. support: if there's conflict code or readme file
4. quality: unit test passed (to be developed)
# Usage
## Installation
```sh
npm install gitcommit.ast
```
## Configure rules
Under your root project. create `.checkrc`
Example
```
{
    "target": ["^test"], 
    "plugins": {
        "conflictCheck":true,
        "ignore":{
            "ignoreCase": true,
            "fileList": ["package-lock.json"]
        },
        "required": {
            "ignoreCase": true,
            "fileList": ["readme"]
        },
        "unittest": {
            "run": "echo 1"
        },
        "nestedFor": {
            "test": "\\.jsx?$",
            "maxdepth": 3
        },
        "nestedIf": {
            "test": "\\.jsx?$",
            "maxdepth": 4
        },
        "customPlugin": {
            "run": "echo 2"
        }
    }
    
}
```
# Pre-defined Features
please checkout some of pre-defined features.
### check if commit msg has jira id if not, get jira id from branch name, otherwise, give error alert
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/commitjira.png)
### check if there's nested if
.checkrc
```sh
{
    "target": ["^test"], 
    "plugins": {
        "nestedIf": {
            "test": "\\.jsx?$",
            "maxdepth": 4
        }
    }]
}
```
Test file: under /test
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/if-test.png)
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/if.png)

### check if there's nested for 
.checkrc
```sh
{
    "target": ["^test"], 
    "plugins": {
        "nestedFor": {
            "test": "\\.jsx?$",
            "maxdepth": 3
        }
    }]
}
```
Test file: under /test
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/for-test.png)
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/for.png)
### check if there's ignore file submitted
.checkrc
```sh
{
    "target": ["^test"], 
    "plugins": {
        "ignore":{
            "ignoreCase": true,
            "fileList": ["package-lock.json"]
        }
    }]
}
```
Test file: under /
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/ignore-test.png)
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/ignore.png)

### check if there's required file which is not submitted
.checkrc
```sh
{
    "target": ["^test"], 
    "plugins": {
        "required": {
            "ignoreCase": true,
            "fileList": ["readme1"]
        }
    }]
}
```
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/required.png)
### check if there's conflict files
.checkrc
```sh
{
    "target": ["^test"], 
    "plugins": {
        "conflictCheck":true
    }]
}
```
Test file: under /test
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/conflict-test.png)
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/conflict.png)
### custom、unittest plugin
.checkrc
```sh
{
    "target": ["^test"], 
    "plugins": {
        "unittest": {
            "run": "echo 1"
        },
        "customPlugin": {
            "run": "echo 2"
        }
    }]
}
```
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/unittest.png)

### A successful result
Running result:
`git commit -m "test"`
![Image text](https://bitbucket.org/lvfengjiao/gitcommit.ast/raw/8bd1d8d2b492dec6702dc52bebe2f0b89a432f1b/screenshot/result.png)